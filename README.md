# Introduction to **`bash`** scripting


> If you have FSL and git installed, you can follow along on your own computer
> by opening a terminal, and cloning this repository:
>
> ```bash
> git clone https://git.fmrib.ox.ac.uk/paulmc/grad-course-unix-scripting.git
> cd grad-course-unix-scripting
> ```


This practical session aims to give a very quick overview of writing and
running `bash` scripts on macOS and Linux operating systems. It does not
specifically focus on writing scripts that use FSL tools, but instead aims to
introduce more general tools and techniques.

The session is structured as follows:

1. Refresher - navigating your file system, and basic UNIX commands
2. Working with plain text
3. Scripting with FSL commands
4. Piping and redirecting outputs
5. Wildcards
6. Variables
7. Command substitution
8. Flow control
9. Writing and running scripts
10. Summary and further reading
11. Exercises

First of all, let's clear up some terminology:

 - **UNIX** is a family of operating systems that were originally developed in
   the 1970s. Linux and macOS provide command-line interfaces that are very
   similar to UNIX, so if you learn how to work at the command-line, you will
   be able to work with both Linux and macOS (and, these days, Windows too,
   via the Windows Subsystem for Linux).

 - A **command-line** is a text-based method of interacting with your
   computer.  It is also often referred to as a *terminal*, *command prompt*,
   or *shell*.

 - **`bash`** is the language (also referred to as a **shell**) that you are
   using when working with Linux or macOS through a command-line. `bash` is a
   successor of an earlier language called `sh`. In fact, there are several
   shell languages in existence - if you are using macOS for instance, you
   might be using a shell called `zsh`. Ubuntu (a popular Linux distribution)
   has its own shell called `dash`. For simple tasks and scripts, the
   differences between the different languages often don't matter. But they
   may become important when you are writing more complicated scripts, in
   which case the best option is to write your script so that it is compatible
   with `bash` or `sh`.


## Refresher - navigating your file system, and basic UNIX commands


> This section introduces some fundamental UNIX commands that you are
> hopefully already familiar with, and some which you may not have
> heard of before.
>
>
>  | Command                  | Description                                          |
>  |--------------------------|------------------------------------------------------|
>  | `pwd`                    | Find out where you are in the file system            |
>  | `cd`                     | Change to a different directory                      |
>  | `ls`                     | List the contents of a directory                     |
>  | `cp`                     | Copy files/directories                               |
>  | `mv`                     | Move/rename files/directories                        |
>  | `mkdir`                  | Create directories                                   |
>  | `rm`                     | Remove files/directories                             |
>  | `basename` and `dirname` | Separate the directory and file components of a path |
>  | `echo`                   | Print to screen                                      |
>  | `find`                   | Find files that match a name pattern                 |
>  | `file`                   | Guess the type of a file                             |
>  | `which`                  | Find the path to an executable command               |
>  | `man`                    | Documentation for built-in UNIX commands             |

Before you can write a script, you need to understand how UNIX file systems
are organised, and understand some of the basic UNIX commands that are
available to you.

Every computer, whether it is a macOS, Windows, or Linux machine, has a
*hierarchical* file system - all of the files on your computer are organised
in a tree. The *root* directory is at the top of the tree, and is denoted by a
forward slash: `/`. **All** of your files are located underneath the root
directory.

<img src=".images/tree.jpg" alt="File system tree" height="200">

> Image from https://www.linuxtrainingacademy.com/linux-directory-structure-and-file-system-hierarchy/

This includes external drives, such as USB and network drives.  Under macOS,
external drives are located within `/Volumes/`. Under Linux, external drives
may be under `/mnt/` or `/media/`. If you are using Linux within the Windows
Subsystem for Linux, Linux has its own root directory at `/`, and your Windows
files are located at `/mnt/c/`.


> Under Windows, you can think of different drives (`C:`, `D:`, etc) as being
> separate trees, each with their own root directory.


### `pwd`: Find out where you are in the file system


When working at the command line, you are always located somewhere in your
file system. You can find out where you are by typing `pwd`, which stands for
*present working directory*.


### `cd`: Change to a different directory


You can move around your file system with the `cd` command, which stands for
*change directory*. You can move to another directory by specifying it either
in terms of your current location (a *relative path*), or in terms of the
root directory (an *absolute path*).

Some symbols can be used as useful shortcuts:
 - `.` refers to the current directory
 - `..` refers to the parent/previous directory, relative to your current
    location
 - `~` refers to your home directory (usually `/Users/yourusername/` on macOS,
   or `/home/yourusername/` on Linux/WSL)

```bash
# Change to the previous directory (using a relative path)
cd ../

# Change to the root directory (using an absolute path)
cd /

# Change to your home directory (using the ~ shortcut)
cd ~

# Change to your home directory (using an absolute path)
cd /home/yourusername

# Change to your Downloads directory (using a relative path)
cd Downloads

# Change to your Documents directory (using a relative path)
cd ../Documents
```

> *Useful tricks*
>
>  - When typing out a file/directory path, you can use the `<tab>` key
>    to automatically complete file/directory names. Doing this is **strongly**
>    recommended: in addition to saving you time by requiring less typing, you
>    can use `<tab>` to verify that the path you have typed is correct - if
>    your path *doesn't* autocomplete, then you know you have made a mistake.
>  - You can use the up and down arrows to step through previously executed
>    commands.
>  - (*Advanced*) You can use `ctrl+r` to search through your commamd history.


### `ls`: List the contents of a directory


The `ls` command allows you to list the contents (files and sub-directories)
of any directory in your file system. By default, `ls` will just print the
names of every item in the directory, but it also has a few useful
options which you can use to customise its output:

 - `-l`: "long" form - also prints sizes of files, modification date, and
   permissions of each item
 - `-h`: "human" form - when combined with `-l`, will print file sizes in
   a more readable format (e.g. using `MB` or `GB` instead of bytes)
 - `-S`: sort items by size (largest first)
 - `-t`: sort items by last modification time (newest first)
 - `-r`: when combined with `-S` or `-t`, reverses the order

```bash
# List the contents of the current directory
ls

# List the contents of the previous directory
ls ..

# List the contents of your home directory
ls ~

# List the contents of your root directory
ls /

# List the contents of your Downloads and Documents directories
ls -l ~/Downloads ~/Documents

# List the Downloads and Documents directories, not their contents
ls -ld ~/Downloads ~/Documents

# List the contents of your Downloads directory, with newest items first
ls -lt ~/Downloads

# List the contents of your Downloads directory, with largest items last
ls -lhrS ~/Documents
```


### `cp`:  Copy files/directories


The `cp` command is used to create copies of files and directories.  The `-r`
option (which stands for *recursive*) is needed when copying directories.

```bash
# create a copy of data.csv, calling it data_copy.csv
cp data.csv data_copy.csv

# create a copy of the dataset directory
cp -r dataset dataset_copy
```


### `mv`:  Move/rename files/directories


The `mv` command is used to move or rename files and directories.

```bash
# Rename the data_copy.csv file to data_backup.csv
mv data_copy.csv data_backup.csv

# Rename the dataset_copy directory to datset_backup
mv dataset_copy dataset_backup

# Move the data_backup.csv file into the dataset_backup directory
mv data_backup.csv dataset_backup
```

### `mkdir`: Create directories


The `mkdir` command is used to create directories. A very useful option to
`mkdir` is `-p`, which allows you to create directories and sub-directories with a
single command:


```bash
# creates a new directory (using a relative path)
mkdir newdir

# creates a new directory (using an absolute path)
mkdir /home/yourusername/newdir

# creates sub-01, and all sub-directories
mkdir -p mydata/imaging/subjects/sub-01
```


### `rm`: Remove files/directories


The `rm` command is used to remove files and directories. Similar to `cp`, the
`-r` option is needed when removing directories.

> *Warning:* Be **very** careful when using `rm`!

```bash
# Remove the data_backup.csv file
rm dataset_backup/data_backup.csv

# Remove the dataset_backup directory, including all of its contents
rm -r dataset_backup
```

### `basename` and `dirname`: Separate the directory and file components of a path


The `basename` command simply removes the directory component of a file path:

```bash
basename dataset/sub-CON01/ses-postop/anat/sub-CON01_ses-postop_T1w.nii.gz
```

`basename` can also remove file extensions (but you have to know the extension
in advance):

```bash
basename dataset/sub-CON01/ses-postop/anat/sub-CON01_ses-postop_T1w.nii.gz .nii.gz
```


The `dirname` command is the complement of `basename` - given a file path, it
will remove the file name, and will return the directory name.

```bash
dirname dataset/sub-CON01/ses-postop/anat/sub-CON01_ses-postop_T1w.nii.gz
```

`basename` and `dirname` come in very useful when you are working with file
paths. For example, you may want to modify the name of a file, but not modify
the path to the directory. You can use `basename` and `dirname` to extract the
two components of the file path, modify the value returned by `basename` to
produce your new file name, and then reconstruct a full path to the new file
by combining the value returned by `dirname` with your new file name.


### `echo`: Print to screen


The `echo` command can be used simply to print something to the terminal. This
sounds trivial, but is very useful in practice, as we will show later on.

```bash
echo "Hello"

# -n tells echo not to end with a new line
echo -n "Hello"

# -e allows the use of escape characters (e.g. new line and tabs)
echo -e "Hello\n\tHow are you?"
```


### `find`: Find files that match a name pattern


The `find` command allows you to search a directory for files which have names
that match a specific pattern. The `dataset` directory contains a pretend BIDS
MRI data set - let's say we want to list the T1 images of all of our control
subjects:

```bash
find dataset -name "*CON*T1w*.nii.gz"
```


### `file`: Guess the type of a file


You may have realised by now that plain text files do not need to have a name
ending with `.txt` - they can have any name at all. If you have a file, and you
are not sure of its contents, the `file` command might come in handy:

```bash
file data.csv
```


### `which`: Find the path to an executable command


UNIX (and FSL) commands are files too - every command that you can run exists
as a file somewhere in your file system<sup>*</sup>. The `which` command can
sometimes come in handy if you are trying to figure out where a particular
command you are using is located (e.g. `python`, if you have more than one
version of Python installed):

```bash
which ls
```

> <sup>*</sup>This is not strictly true - some UNIX commands (e.g. `cd`) are
> built into the shell language, so don't exist as files.


### `man`: Documentation for built-in UNIX commands


UNIX environments have a built-in documentation system, with comprehensive
documentation for all built-in commands such as `ls`, `cd`, and `cut`
(introduced below). For example, typing `man ls` will bring up the
documentation (a.k.a. *manpage*) for the `ls` command.

> Note that software which you install yourself may not provide documentation
> in the form of manpages. For example, FSL does not have manpages - instead,
> the FSL documentation is hosted online at
> https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/.


## Working with plain text


> This section introduces some UNIX commands which, if you can master them,
> will get you most of the way to being able to claim that you are a "Data
> Scientist":
>
>  | Command           | Description                               |
>  |-------------------|-------------------------------------------|
>  | `cat`             | Print the contents of a file              |
>  | `paste`           | Join files together line-by-line          |
>  | `less`            | Show the contents of a file               |
>  | `head` and `tail` | Print the start or end of a file          |
>  | `wc`              | Count the number of lines/words in a file |
>  | `sort`            | Sort lines in a file                      |
>  | `uniq`            | Remove duplicate lines from a file        |

The UNIX command line is a very convenient environment for working with and
manipulating *plain text* files. Plain text files are exactly what they sound
like - they are files which simply contain letters, numbers, and punctuation
symbols, without any information about formatting.

> Unicode-encoded plain text files can also contain other characters (e.g.
> characters from non-English languages, and even emojis), but for the sake of
> simplicity we are not considering them in this session.

For example, many (non-imaging) data sets come in the form of plain text
files, with a file name ending in `.txt`, `.csv` (*comma-separated value*) or
`.tsv` (*tab-separated value*). UNIX has a number of commands which makes
working with these types of files very easy.

Each of these commands perform one simple task which, taken individually, may
not seem particularly useful. But the real power comes in combining these
commands together - this is covered below in the section on *Piping and
redirecting*.

> *Note*: Many of the commands we introduce in this session are **much** more
> powerful than what is demonstrated here (`find`, `grep`, and `sed` in
> particular) - check out their `man` pages if you are interested.


### `cat`: Print the contents of a file


The `cat` command (short for *concatenate*) simply prints the contents of a
file. If you are working with small files, which you know only contain a few
lines in them, `cat` is a simple way to view their contents:

```bash
cat data.csv
```

`cat` can also be used to join files together. Sometimes you might be given a
dataset where the data is in one file, but the column labels are in another
file. You can use `cat` to combine them together:


```bash
cat columns.txt data.txt
```


### `paste`: Join files together line-by-line


We just saw that we can use `cat` to concatenate column labels and data from
separate filse together. If we have *row* labels stored in another file,
we can use another UNIX command called `paste` to join the row
labels and data together - we can use the `-d` option to specify the column
separator/delimiter:

```bash
paste -d , ids.txt data.txt
```

> *Useful shortcuts when editing commands*
>  - You can type `ctrl+a` to move the cursor to the beginning of the line,
>    and `ctrl+e` to move the cursor to the end.
>  - You can use `alt+f` and `alt+b` to move the cursor forwards and backwards
>    one word at a time.
>  - You can use `alt+backspace` to delete a word behind the cursor, and `alt+d`
>    to delete a word in front of the cursor.
>  - You can use `ctrl+k` to cut ("kill") everything in front of the cursor,
>    and `ctrl+y` to paste ("yank") it back.


### `less`: Show the contents of a file


If you have a larger file, the `less` command allows you to view its contents
interactively - typing `less data.csv` will show the contents of `data.csv`,
one screen at a time. You can use the up and down arrows to navigate through
the file (or use alt+v or ctrl+v to go up or down one screen at a time), and
`q` to quit.

> You may also see reference to another command called `more`, which is very
> similar to `less` - you can think of them as being more or less equivalent
> (pun intended).


### `head` and `tail`: Print the start or end of a file


If you have a large file, and you just want to get a feel for its contents, you
can use `head` and `tail` to peek at the first few, or last few, lines of a file:

```bash
head      data.csv  # Print the first 10 lines of the file
tail      data.csv  # Print the last 10 lines of the file
head -n5  data.csv  # Print the first 5 lies of the file
tail -n5  data.csv  # Print the last 5 lies of the file
head -n-5 data.csv  # Print all but the last 5 lines of the file
tail -n+5 data.csv  # Print all but the first 5 lines of the file
```


### `wc`: Count the number of lines/words in a file


If you don't know how big a file is, you can use the `wc` command to count the
number of lines it contains. If you have a space- or tab-separated file, you
can also count the number of "words" in the file.

```bash
wc    data.csv # Print the number of lines, words, and characters in the file
wc -l data.csv # Print the number of lines in the file
wc -w data.csv # Print the number of words in the file
```


### `sort`: Sort lines in a file


The `sort` command does exactly what you think it does - it sorts the lines
of a file. By default, `sort` will sort alphabetically, but it has several
options, including the ability to sort numbers:

```bash
sort     names.txt   # Sort some names alphabetically
sort     numbers.txt # Sort some numbers alphabetically (not very useful)
sort -n  numbers.txt # Sort some numbers numerically (better)
sort -rn numbers.txt # Sort some numbers numerically, largest first
```

When using `sort`, and in general, you need to take care when working with
alpha-numeric codes (e.g. `CON01` and `PAT03` to denote controls and
patients). Most tools are unable to sort alpha-numeric codes in the way that
you might expect, because they don't know anything about the format of your
codes.  This means that ordering-related errors can be very easy to make
(e.g. where you assume that the order of a list of file paths matches the
order you have used elsewhere, e.g. in an associated data file, or a GLM
design matrix).

As an example, `badcodes.txt` contains some poorly formatted alpha-numeric
codes:

```bash
sort badcodes.txt
```

The `sort` command is unable to sort these codes according to the numeric part
of the code. If, however, we were to use zero-padded codes, `sort` would
produce a more sensible ordering:

```bash
sort goodcodes.txt
```


### `uniq`: Remove duplicate lines from a file


The `uniq` command can be used to count, or to filter repeated or duplicate
lines in a file.

```bash
uniq    names.txt # Print the unique lines of a file
uniq -c names.txt # Print the unique lines of a file, along with their counts
uniq -u names.txt # Only print lines which did not have duplicates
uniq -d names.txt # Only print lines which did have duplicates
```

`uniq` will only consider consecutive lines as duplicates, so it is usually
not very useful on its own. But `uniq` is much more useful when combined with
`sort`, as we shall see shortly.


## Scripting with FSL commands


FSL commands can be used just like any other UNIX command, including the ones
introduced in this overview - you can call FSL commands from scripts and save
the information that they output as variables.

> Remember that the documentation for FSL commands cannot be accessed with
> `man`, as is the case with built-in UNIX commands - many FSL commands will
> print out some help information when you call them without any parameters
> (e.g. try running `bet`, `flirt`, etc). There is also a wealth of
> documentation and information on the [FSL
> wiki](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/).

Some examples of FSL commands which are commonly used in scripts are:

| Command      | Description                                                                                        |
|--------------|----------------------------------------------------------------------------------------------------|
| `fslstats`   | Calculate summary statistics (e.g. mean, minimum and maximum voxel intensity) for a NIFTI image    |
| `fslmaths`   | Manipulate and combine NIFTI images with arithmetic operations                                     |
| `cluster`    | Print information about contiguous clusters of voxels in a NIFTI image                             |
| `imcp`       | Copy NIFTI image files without having to worry about the file extension (e.g. `.nii` or `.nii.gz`) |
| `imtest`     | Test whether a NIFTI image file exists, without having to worry about the file extension           |
| `remove_ext` | Remove the file extension from a NIFTI image file name                                             |

Many FSL commands are actually scripts themselves, so you can use `less` to
view their contents and see what they do (because scripts are plain-text
files):

```bash
less $(which fsl_anat)
```

> Above we used *command substitution*, to pass the output of the `which`
> command to the `less` command. We will talk more about command substition
> shortly.

We will show some examples of using FSL commands in the remainder of this
practical.


## Piping and redirecting outputs


> This section introduces the magical pipe (`|`) and redirect (`>`) operators,
> along with some more advanced UNIX commands:
>
>  | Command | Description               |
>  |---------|---------------------------|
>  | `grep`  | Search for text           |
>  | `sed`   | Replace text              |
>  | `tr`    | Replace/delete characters |
>  | `cut`   | Extract columns           |


So far, most of our examples have involved us passing the name of a file to a
particular command. The command will then read the file, perform its task, and
print its output. This method of working is somewhat limiting - it would be
much nicer if we could choose our input file, and then specify several
commands to run, all at once. We can accomplish this with *piping*.

Piping allows us to connect, or pipe, the output of one command into the input
of another command. Piping is the main reason that UNIX is such a powerful
environment for working with text.

You can pipe the output of one command into another by using the pipe `|`
operator. For example, let's say we have a file of numbers, and we want to
view the 10 largest numbers. We can do this easily by combining `cat`, `sort`
and `head`:

```bash
sort -n numbers.txt | head

# or, equivalently
cat numbers.txt | sort -rn | head
```

You can see that we have a duplicate in our results, but we actually want the
10 largest *unique* numbers. Simple - just add `uniq` into our pipeline:

```bash
cat numbers.txt | sort -rn | uniq | head
```

But we are still missing one step - what if we wanted to save those numbers to
a file, so we could load them into some other software and analyse them?  This
is where the *redirect* operator (`>`) comes into play - we can use it to
redirect the output of *any* command to a file.

```bash
cat numbers.txt | sort -rn | uniq > sorted_unique_numbers.txt
```

We now have a new file `sorted_unique_numbers.txt`, containing the same data
as `numbers.txt`, but sorted in decreasing order, with duplicates removed.

The `>` operator will always create a new file - if a file with the specified
name already exists, it will be overwritten. In contrast, the `>>` operator
allows you to *append* to an existing file. This can be useful for
e.g. logging, or for incrementally creating a data file:

```bash
fslstats dataset/sub-PAT01/ses-postop/anat/sub-PAT01_ses-postop_T1w.nii.gz -m >> image_means.txt
fslstats dataset/sub-PAT02/ses-postop/anat/sub-PAT02_ses-postop_T1w.nii.gz -m >> image_means.txt
fslstats dataset/sub-PAT03/ses-postop/anat/sub-PAT03_ses-postop_T1w.nii.gz -m >> image_means.txt
fslstats dataset/sub-PAT04/ses-postop/anat/sub-PAT04_ses-postop_T1w.nii.gz -m >> image_means.txt
fslstats dataset/sub-PAT05/ses-postop/anat/sub-PAT05_ses-postop_T1w.nii.gz -m >> image_means.txt
```


### `grep`: Search for text


The `grep` command allows you to search for words, or patterns, in a file. For
example, you may have a file which contains the birth dates of a group of
individuals, and you want to find those who were born in 1958:

```bash
grep 1958 data.csv
```

We can also use the `grep -v` option to *remove* lines which match our
pattern:

```bash
grep -v 1958 data.csv
```

A common task when working with data files is to count the number of
occurrences of some value or pattern.  Let's say that we want to count the
number of inviduals who were born in 1994. We can do this by combining `grep`
with `wc`:

```bash
cat data.csv | grep 1994 | wc -l

# or, equivalently
grep 1994 data.csv | wc -l
```

If you are searching for text, `grep` will perform a case-sensitive search by
default:

```
cat names.txt | grep Al
```

But you can use the `-i` option to perform a case-insensitive search:

```
cat names.txt | grep -i al
```


### `sed`: Replace text


Like `grep`, the `sed` command allows you to search for patterns in data. But
`sed` goes one step further, and lets you *replace* text. You specify some
text to search for, and specify the text you want to replace it with, with the
syntax `s/text_to_search_for/text_to_replace_it_with/g`. For example, you
might want to replace the `dob` column title with `date-of-birth`:

```bash
cat data.csv | sed 's/dob/date-of-birth/g'

# to save the relabelled version to a new file
cat data.csv | sed 's/dob/date-of-birth/g' > data_relabelled.csv
```

> *Note:* A little known fact about `sed` is that you can use any character,
> not just `/`, in the replacement command. For example, we could have written
> `s@dob@date-of-birth@g`.


### `tr`: Replace/delete characters


`tr` (short for *translate*) allows you to remove or replace specific
characters in a file. It can be very useful for reformatting data files. For
example, you might have some data in a `.csv`file, but the tool that you want
to use to analyse it will only accept `.tsv` file. You can easily reformat the
file using `tr`:

```bash
cat data.csv | tr ',' '\t'
```

`tr` has a few options, one of which is `-s`, which allows you to *squeeze*,
or remove duplicate characters. For example, sometimes you might come across
data files which are stored in a *fixed width* format, where each column is
aligned vertically, rather than being separated by a delimiter character such
as `,` or `\t`:

```bash
cat fixedwidth.txt
```

`tr` can easily convert data like this into, for example, comma-separated
data:

```bash
cat fixedwidth.txt | tr -s ' ' | tr ' ' ','

# To save the result to a new file
cat fixedwidth.txt | tr -s ' ' | tr ' ' ',' > commasep.txt
```


### `cut`: Extract columns


Another very useful command, when working with tab- or comma-separated data,
is `cut`. The `cut` command can be used to extract specific columns from a
file. For example, we might only be interested in the date of birth of each
individual from our data set.

We can use the `-d` option to tell `cut` how columns are separated, and the
`-f` option to tell `cut` which column we want (starting from `1`):

```bash
cat data.csv | cut -d , -f 2

# We can use tail to get rid of the column label from the first line
cat data.csv | cut -d , -f 2 | tail -n+2
```


## Wildcards


Now that we know about some of the fundamental commands that are available to
us in UNIX, we can start looking at some of the more sophisticated features
in `bash`.

Wildcards are a feature which allow you to identify files or directories with
names that match a pattern. For example, the wildcard pattern `*.txt` will
match anything that ends in `.txt`, and the wildcard pattern `*CON*` will
match anything that contains `CON`.

For example, we can use a wildcard to list all of the `2mm` standard template
images that come with FSL:

```bash
ls -l ${FSLDIR}/data/standard/*2mm*.nii.gz
```

> In this command, `${FSLDIR}` is a *variable* - these are covered in the next
> section.

Or, we could use a wildcard to list all of the data set directories for our
patient group:

```bash
ls -dl dataset/*PAT*
```

The use of wildcards are one reason why you should put some thought into the
way that you organise your data sets. You should never make any assumptions
about the order in which wildcards, `ls`, `find`, or any other UNIX command
will print file names or directories. The ordering will potentially be
different on different OSes or shells. This means that it is very easy to make
subtle mistakes, for example where you have assumed that the order of files
returned by a wildcard pattern matches the order that you have used in a FEAT
design matrix.

The `sort` command comes in handy here - its default behaviour is to sort
alphabetically (where numbers are treated like letters, and are sorted after
letters). So if your files and directories are named and organised sensibly,
you can always enforce a consistent ordering across all environments by piping
through `sort`:

```bash
ls -dl dataset/*PAT* | sort
```

The `badnaming` directory contains an example of a poorly organised data set:

```bash
ls -dl badnaming/*CON* | sort
```

The `sort` command is not smart enough to sort the letters separately from the
numbers, so this dataset is much more awkward to work with. By far the easiest
approach is to name and organise your data so that it can be sorted
alphabetically.


## Variables


If you have some experience in any kind of scripting or programming, you will
hopefully be familiar with the concept of a *variable* - in simple terms, a
variable is just a label used to store, or refer to, a value. All variables in
`bash` are *strings* (character sequences) - if you are doing anything
non-trivial with numbers, you could use the `bc` command, or (better), use a
more suitable programming language such as Python.

You can assign a value to a variable using an equal sign (`=`), like so:

```bash
var="value"
```

> Note that there **cannot** be any spaces around the equal sign, i.e.
> `var = "value"` is invalid.


Once you have created a variable, you can refer to it using `${var}`:

```bash
echo ${var}
```

The curly braces are optional, but using them is safer in many situations, and
usually makes code easier to read. But this works too:

```bash
echo $var
```

The reason that curly braces are safer is that they allow `bash` to know where
the variable name ends - this is useful when we are using variables to
construct strings. In the second line of the code below, `bash` thinks that we
are referring to a variable called `$var1_brain` (which is a valid variable
name):

```bash
var1=struct
var2=$var1_brain   # bug!
echo $var2.nii.gz
```

If we change the code to use curly braces, we can achieve what we were trying
to do:

```bash
var1=struct
var2=${var1}_brain
echo ${var2}.nii.gz
```


## Command substitution


When writing a `bash` script, you will often want to capture the output of a
command, and store it in a variable. For example, you might want to store the
number of lines in a text file using the `wc` command. You can do this with
*command substitution*, by enclosing the command in `$()`:

```bash
numlines=$(wc -l numbers.txt)
echo $numlines
```

Command substitution is also possible using back-ticks (the `` ` ``)
character), for example:

```bash
numlines=`wc -l numbers.txt`
echo $numlines
```

However, the `$()` style is preferable, as it is clearer, supports nesting, and
is easier to copy/paste.

When working with NIFTI images, you will often see related files being given
names with a common file prefix. For example, if your T1 image is called
`struct.nii.gz`, the brain-extracted version might be called
`struct_brain.nii.gz`, and the brain mask `struct_brain_mask.nii.gz`.

The FSL `remove_ext` command can be used to remove the extension (typically
`.nii` or `.nii.gz`) from a NIFTI image file. For example, we could write a script
which uses `remove_ext`, and then passes the resulting file prefix to `bet`:

```bash
niftifile="struct.nii.gz"

# returns "struct"
fname=$(remove_ext ${niftifile})

# will create struct_brain.nii.gz and struct_brain_mask.nii.gz
bet ${fname} ${fname}_brain -m
```


## Flow control


All but the most trivial of scripts will need some form of *flow control* -
the ability to run code on the basis of some condition, or to run a block of
code repeatedly, for example to apply a set of commands to the data for
different subjects. Like with many other programming languages, the two
fundamental flow control constructs in `bash` are `for`-loops, and `if-else`
statements.


### `for`-loops


If you have a command (or a set of commands) that you need to run many times,
e.g. once for each subject, you can use a `for` loop. For example, this code
runs `bet` on the T1 image of every subject in our data set:

```bash
# "subjectdir" is a variable which, on each
# iteration of the for loop, will be set to
# the next match from the dataset/sub-* wildcard.
for subjectdir in dataset/sub-*; do

    # store the subject ID (e.g. "sub-CON01")
    # in a separate variable called "subject"
    subject=$(basename $subjectdir)

    # construct file paths to the
    # input and output files
    input=${subjectdir}/ses-postop/anat/${subject}_ses-postop_T1w.nii.gz
    output=$(remove_ext $input)_brain

    echo "Running bet $input $output ..."
    bet $input $output
done
```


### `if-else` statements


You will often need to run a command *conditionally*, i.e. only if some
condition is met. We can do this using the `if` and `else` statements. For
example, we can enhance our code above so that it only runs `bet` if it has
not already been run, using the FSL `imtest` command:

```bash
for subjectdir in dataset/sub-*; do

    # store the subject ID (e.g. "sub-CON01")
    # in a separate variable called "subject"
    subject=$(basename $subjectdir)

    # construct file paths to the
    # input and output files
    input=${subjectdir}/ses-postop/anat/${subject}_ses-postop_T1w.nii.gz
    output=$(remove_ext $input)_brain

    # if the output file already
    # exists, don't run bet again
    if [[ $(imtest $output) == "1" ]]; then
        echo "bet has already been run on $input - skipping"
    else
        echo "Running bet $input $output ..."
        bet $input $output
    fi
done
```

The `if` statement can be used to perform a wide range of tests, including

 - Comparing command outputs or variables with string constants (like what we
   did above)
 - Testing properties of files and directories (e.g. does a file exist or not)
 - Testing properties of variables (e.g. whether a variable exists, or is empty)
 - Integer comparison (for variables which contain values that can be
   interpreted as integer numbers)

For historical reasons, the syntax that is used in `if` statement tests comes
in many forms. So when you are trying to write a test with an `if` statement,
a good resource to have on hand is the **Tests** section of the [*Advanced
BASH scripting guide*](https://tldp.org/LDP/abs/html/tests.html), which
provides a comprehensive overview of the different ways that the `if`
statement can be used.


## Writing and running scripts


So far, we have been running code within an interactive `bash` shell.  But
everything that you can run in an interactive shell can be placed in a
plain-text file, and executed as a script. Once you have saved your script,
you can re-run it as many times as you like.

A `bash` script needs to be written with a *plain text* editor - you can't use
Microsoft Word, for example. You can use any plain text editor you want - on
macOS, you can use TextEdit, but you need to make sure that the file gets
saved without any formatting information. An editor called `gedit` is
installed on most Linux systems. Or, if you don't mind editing files inside a
terminal, you can use `nano`, `emacs` or `vi`.

Here is a complete example of a working, but not very useful, script, called
`myscript`:

```bash
#!/usr/bin/env bash
name=$1
echo "Hello, ${name}!"
```

Before we can run this script, a few things need explaining...


### The shebang line


The very first line in every bash script should be:

```
#!/usr/bin/env bash
```

This line is referred to as the *shebang*, or *hash-bang*, and it tells your
OS that your script should be executed as a `bash` script. You will often see
variants of this line, including `#!/bin/bash` `#!/bin/sh`, and
`#!/usr/bin/bash`, but using `#!/usr/bin/env bash` gives the best
compatibility across different systems.

> Historically `#!/bin/sh` was recommended, but this was before `bash` became
> the de-facto standard on most OSes, and these days using `#!/bin/sh` can
> cause errors - `bash` is a super-set of `sh`, so if you have used some
> `bash`-specific features in your script, and it is executed with `sh`
> instead of `bash`, you may experience problems.

With the exception of the shebang line, all other lines that begin with a `#`
character are treated as comments, and ignored.


### Making a script executable


When you are ready to run your script, you need to make it executable. Every
file in a UNIX file system is managed by a permissions system, that allows you
to specify whether the file is readable (`r`), writable/editable (`w`), and
executable (`x`). When you create a new file, it will usually be readable and
writable, but not executable. You can make a file executable using the `chmod`
command. To make our `myscript` script executable, we can run:

```bash
chmod a+x myscript
```

Now `myscript` is executable - we can run it simply by referring to it with a
relative or absolute path:

```bash
./myscript
```


### Command-line arguments


In order to do anything useful, most scripts will need to be provided with
some input arguments, for example the name of a NIFTI image file, or a subject
ID. Our example script `myscript` expects to be passed one input argument - in
other words, when we run it, we should pass it an argument like this:

```bash
./myscript Paul
```

If we look at `myscript`, we can see that the `name` variable is set to the
value of another variable - `$1`. When you run a script, all of the arguments
that are passed to the script are assigned to numbered variables `$1`, `$2`,
`$3`, etc.  So when we ran `./myscript Paul`, the variable `$1` was set to the
value `"Paul"`.

You can use these argument variables just like any other - for example, we
could simplify `myscript` (at the cost of making it slightly less readable) by
using the variable `$1` directly:

```bash
#!/usr/bin/env bash
echo "Hello, $1!"
```

> Some additional argument variables are available in all scripts:
>  - `$0` - The name of the script (`"./myscript"` in the above invocation)
>  - `$#` - The number of arguments (not including the script name)
>  - `$@` - All arguments that were passed to the script


### Running your script from anywhere with the `$PATH` variable


When you write a script and make it executable, you can run it by referring to
it via a relative or absolute path, e.g.:

```bash
./myscript Paul
```

But it is possible to run built-in and FSL commands without knowing where they
are located, e.g.:

```bash
ls
bet
```

This is possible because of a built-in variable called `$PATH`. The `$PATH`
variable is a list of directories, separated with `:` characters. You can look
at the contents of the `$PATH` variable on your own computer like so:

```bash
echo $PATH
```

Whenever you type the name of a script or command into your shell without
using a relative or absolute path, it consults the `$PATH` variable - it
searches every directory on the `$PATH` to see if there is a command with the
name you typed contained within. So by adding `$FSLDIR/bin/` to the `$PATH`,
we are able to run all FSL commands without having to explicitly refer to
their location.

You can use `$PATH` to run your own scripts too:

```bash
./myscript Paul             # Run myscript using a relative path
myscript Paul               # Error!
export PATH=$(pwd):${PATH}  # Add the current directory to the $PATH
myscript Paul               # myscript is now on the $PATH, so now this works!
```


## Summary and further reading


The aim of this session was to give you a very brief, practical overview of
writing and running `bash` scripts on macOS and Linux systems. It covered a
range of built-in UNIX commands and commands provided by FSL, pipes and
redirection, wildcards, variables, command substitution, and the practical
aspects of how to save, run and manage script files.


A vast collection of information on `bash` scripting can be found on the
internet. Here are a few sites that will keep you busy for a while:

 - The FSL course material provides a very good tutorial on scripting, with
   many FSL-specific examples:
   https://open.win.ox.ac.uk/pages/fslcourse/lectures/scripting/
 - The *Bash Guide for Beginners* is a great place to start:
   https://tldp.org/LDP/Bash-Beginners-Guide/html/
 - *The Bash Guide* gives a detailed overview of `bash`:
   https://guide.bash.academy/
 - *The Advanced Bash Scripting Guide* is the definitive reference:
   https://tldp.org/LDP/abs/html/


## Exercises

These exercises will help you consolidate the information that you have
learned in this session, and will hopefully give you some insight into how
easy certain tasks can be given just a little expertise in scripting.

1. Write a script called `csv_to_tsv`, which is given a file name as an input
   argument, and which takes the file, converts all commas to tabs, and prints
   the result.

2. Write a script called `print_line`, which is given the name of a file, and a
   line number as input arguments, and which prints just that line from the
   file.

3. Write a script called `csv_header`, which is given the name of a CSV file
   as an input argument, and which prints the header of each column on a new
   line.

4. Write a script called `sort_column` which is given as arguments:
    - the name of a comma separated file
    - the name of an output file
    - a column number

   and which extracts the appropriate column, sorts the values, and
   saves them to the output file

5. Write a script called `word_histogram` which is given as arguments:
    - the name of a comma separated file
    - the name of a word to search for

   and which prints out the number of lines which contain the word

6. Modify your script from the previous exercise (create a copy called
   `unique_word_histogram`) to count the number of *unique* lines that contain
   the search word.

7. Write a script called `shape`, which is given the name of a comma-separated
   file as an argument, and which calculates and prints the number of rows and
   the number of columns contained in the file (you can assume that every row
   contains the same number of columns).

8. Write a script called `label_data`, which is given as arguments:
   - The name of a CSV data file which does not contain row or column labels
   - The name of a file containing row labels
   - The name of a file containing column labels
   and which prints out the CSV data, with both row and column labels.

9. Write a script called `find_nifti`, which is given as arguments:
   - the name of a directory
   - a sequence of characters to search for

   and which finds and prints the names of all `.nii.gz` files in the
   directory which contain the search term in their name.

10. Write a script called `nifti_ranges`, which is given as arguments:
    - the name of a directory
    - a sequence of characters to search for

    and which finds all `.nii.gz` files in the directory which contain the
    search term in their name, then for each file runs `fslstats <file> -R` to
    calculate the minimum and maximum voxel intensities, and then prints the
    file name and minimum and maximum values.

11. Write a script called `rename_nifti`, which is given as arguments:
    - the name of a directory
    - a sequence of characters to be replaced
    - a sequence of characters to replace them with

    and which finds every `.nii.gz` file in the directory, and renames each
    file such that the first sequence of characters is replaced with the second
    sequence. For example, you might use this script to rename files of the
    form `sub-CON02_ses-postop_T1w.nii.gz` to
    `sub-CON02_ses-postoperation_T1w.nii.gz` like so:
       `rename_nifti dataset postop postoperation`

12. Write a script called `relabel_dataset` which is given the name of a
    directory as an argument, and which:

    - Finds all `.nii.gz` files contained within the directory
    - Renames any files that start with `sub-CON` so that they start with
      `sub-CONTROL`
    - Renames any files that start with `sub-PAT` so that they start with
      `sub-PATIENT`

    For example, a file called `sub-CON01_ses-postop_T1w.nii.gz` would be
    renamed to `sub-CONTROL01_ses-postop_T1w.nii.gz`.
